export default [
    {
        id: '150',
        name: 'iPhone6',
        status: 'processing',
    },
    {
        id: '151',
        product: 'iPad Pro',
        status: 'sent',
    },
    {
        id: '300',
        product: 'Microsoft surface',
        status: 'processing',
    },
    {
        id: '320',
        product: 'Galaxy S7 edge',
        status: 'processing',
    },
    {
        id: '501',
        product: '128G SD Card',
        status: 'delivered',
    },


];